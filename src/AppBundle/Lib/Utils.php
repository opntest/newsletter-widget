<?php
namespace AppBundle\Lib;

use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Utils
 *
 * This is a utility class, a special case of a helper class, a collection of useful methods for the application
 */
class Utils
{
    /**
     * isJSON
     *
     * This method checks if the given parameter is a valid Json string
     *
     * @param string $string (the string to be checked)
     *
     * @return boolean
     */
    public static function isJSON($string)
    {
        return is_string($string) && is_array(json_decode($string, true)) ? true : false;
    }

    /**
     * JSredirect
     *
     * This method returns a Javascript redirection block
     *
     * @param string $link (redirect Url)
     *
     * @return string
     */
    public static function JSredirect($link)
    {
        echo "<script type='text/javascript'>";
        echo "document.location='" . $link . "';";
        echo "</script>";
    }
}

