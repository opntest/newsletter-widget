<?php
namespace AppBundle\Lib;

use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Users
 *
 * This class implements methods for the 'Users' section
 */
class Users
{
    protected $_dbconn = null;

    /**
     * @constructor
     */
    public function __construct($conn)
    {
        $this->_dbconn = $conn;
    }

    /**
     * getUserByEmail
     *
     * This method gets an User by email
     *
     * @param string $email
     *
     * @return array
     */
    public function getUserByEmail($email)
    {
        $sql = "SELECT * FROM newsletter WHERE email = '".$email."'";
        $stmt = $this->_dbconn->query($sql);
        $result = $stmt->execute();
        $results = $stmt->fetchAll();

        return $results;
    }

    /**
     * saveSubscription
     *
     * This method handles the action of saving a new newsletter subscription
     *
     * @param array $formData (posted form data)
     *
     * @return boolean
     */
    public function saveSubscription($formData)
    {
        if($formData['email']) {
            try {
                $sql = "INSERT INTO newsletter (email) VALUES ('".$formData['email']."')";
                $stmt = $this->_dbconn->query($sql);

                return true;

            } catch (\Exception $e) {
                error_log($e->getMessage());
            }
        }
        return false;
    }
}

