<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\DBAL\Connection;
use AppBundle\Lib\Users;
use AppBundle\Lib\Utils;

class NewsletterController extends Controller
{
    public function __construct(Connection $conn)
    {
        $this->users = new Users($conn);
    }

    /**
     * @param Request $request
     *
     * @Route("/subscribe", name="AppBundle_subscribe")
     * @Method({"GET", "POST"})
     * @Template("@App/subscribe.html.twig")
     *
     * @return Response
     */
    public function subscribeAction(Request $request)
    {
        return array(
            'pageVars' => array('save_subscription_url' => '/subscribe/save')
        );
    }

    /**
     * Save subscription
     *
     * This method handles the action of saving a newsletter subscription
     * @param Request $request
     * @Route("/subscribe/save", name="AppBundle_subscribe_save")
     * @Method({"GET", "POST"})
     *
     * @return JsonResponse
     */
    public function saveAction(Request $request)
    {
        //Default response
        $response = array('status' => 'fail', 'error' => null, 'redirectUrl' => null);
        $errors = array();
        $error = '';

        if ($_POST) {
                $requiredFields = array('email');

                try {
                    $postedFormData = array();
                    $emptyRequiredFields = array();

                    foreach ($_POST as $fieldName => $value) {
                        $postedFormData[$fieldName] = $value;

                        //Required fields validation
                        if ( in_array($fieldName, $requiredFields) && empty($value) ) {
                            $emptyRequiredFields[] = $fieldName;
                        }
                    }

                    if (!empty($emptyRequiredFields)) {
                        $errors['required_fields'] = 'Required fields:' . implode('<br /> ', $emptyRequiredFields);
                    } else {
                        //Email validation
                        if(filter_var($postedFormData['email'], FILTER_VALIDATE_EMAIL) === false) {
                            $errors['required_fields'] = 'This email address is invalid.';
                        }
                    }

                    if (!$errors) {
                        //Check for duplicates
                        $existingUser = $this->users->getUserByEmail($postedFormData['email']);

                        if($existingUser) {
                            $errors['required_fields'] = 'This email address has already subscribed.';
                        } else {
                            //Save the subscription
                            if($this->users->saveSubscription($postedFormData)) {
                                $response['status'] = 'success';
                                $response['redirectUrl'] = '/';
                            }
                        }
                    }
                } catch (\Exception $e) {
                    $this->get('logger')->error('Exception - Unable to save the newsletter subscription: ' . $e->getMessage());
                }
        }

        if ($errors) {
            $error = implode(
                '<br />',
                $errors
            );
        }

        return new JsonResponse(array(
            'status' => $response['status'],
            'error' => $error,
            'redirectUrl' => $response['redirectUrl']
        ));
    }
}
