Newsletter Widget
========================

A Bootstrap Newsletter widget that allows users to subscribe to a newsletter.


Installation

1. Run the following command at your command line: git clone https://cpaopn@bitbucket.org/opntest/newsletter-widget.git 
2. Create the database according to the database/db.sql file. 
3. Run the following command at your command line: composer install
4. Start the Symfony server from the command line:
    cd new-project/
    symfony server:start 
5. Run the application in browser: /subscribe

